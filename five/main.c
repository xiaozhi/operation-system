//
//  main.m
//  five
//
//  Created by leochen on 2021/9/14.
//

//#import <Foundation/Foundation.h>
#include <stdio.h>
#include <unistd.h>


void test1(void);
void test2(void);

int main(int argc, const char * argv[]) {
    for (int i = 0; i < argc; i++) {
        printf("param = %s\n", argv[i]);
    }
    if (argc == 1) {
        test1();
    } else {
        test2();
    }
    return 0;
}

void test1() {
    int pid = fork();
    int x = 100;
    if (pid < 0) {
        printf("fork error\n");
    } else if (pid == 0){
        printf("child, x %d, pid= %d\n", x, pid);
    } else {
        printf("parent, x %d, pid= %d\n", x, pid);
    }
}

void test2() {
    int pid = fork();
    int x = 100;
    if (pid < 0) {
        printf("fork error\n");
    } else if (pid == 0){
        printf("child before, x %d, pid= %d\n", x, pid);
        x = 88;
        printf("child after, x %d, pid= %d\n", x, pid);
    } else {
        printf("parent before, x %d, pid= %d\n", x, pid);
        x = 13;
        printf("parent after, x %d, pid= %d\n", x, pid);
    }
}
